import subprocess
from portmod.progress import wait_subprocess
from portmod.config import read_config

from pybuild import Pybuild1, InstallDir, File, Source, ModInfo

ModInfo()


class Mod(Pybuild1):
    NAME = "OpenMW Leveled List Fixer"
    DESC = "Utility that automatically merges levelled lists from other mods"
    HOMEPAGE = "https://github.com/jmelesky/omwllf"
    LICENSE = "ISC"
    KEYWORDS = "openmw"
    REBUILD = "ANY_PLUGIN"
    SOURCES = [
        Source(
            "https://github.com/jmelesky/omwllf/archive/v{}.tar.gz".format(MV),
            "omwllf-{}.tar.gz".format(MV),
        )
    ]
    INSTALL_DIRS = [InstallDir(M, PLUGINS=[File("OMWLLF.omwaddon")])]

    def prepare(self):
        print("Generating OMWLLF.omwaddon...")
        archive_path = "omwllf-{}.tar.gz/omwllf-{}/".format(MV, MV)
        process = subprocess.Popen(
            [archive_path + "omwllf.py", "-m", "OMWLLF.omwaddon", "-d", archive_path],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            bufsize=1,
        )
        config = read_config()
        remaining = config.get("content")
        initial = len(remaining)

        def update(line):
            for plugin in remaining:
                if plugin in line:
                    remaining.remove(plugin)
            return len(remaining)

        wait_subprocess(process, update, len(remaining))
