# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from pybuild import Pybuild1, InstallDir, File, Source
from pybuild.morrowind import MOD_DIR, read_config, write_config, add_use


class Mod(Pybuild1):
    NAME = "Morrowind"
    DESC = "Base morrowind data files by Bethesda"
    HOMEPAGE = "https://elderscrolls.bethesda.net/en/morrowind"
    SOURCES = []
    KEYWORDS = "openmw tes3mp"
    IUSE = "bloodmoon tribunal"
    TIER = "0"
    LICENSE = (
        "all-rights-reserved"
    )  # Technically there is a specific EULA, however the user would have accepted it already when installing the files.
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",  # First argument must be the Path of the data directory within the archive. All other arguments are optional.
            PLUGINS=[
                File("Morrowind.esm"),
                File("Bloodmoon.esm", USE="bloodmoon"),
                File("Tribunal.esm", USE="tribunal"),
            ],
            ARCHIVES=[
                File("Morrowind.bsa"),
                File("Bloodmoon.bsa", USE="bloodmoon"),
                File("Tribunal.bsa", USE="tribunal"),
            ],
        )
    ]

    # Morrowind must exist on your machine already. All this does is find it and pretend that we installed it
    # NOTE: Most mods should not do this! Manually adding USE flags and doing file system operations
    #    outside the INSTALL_DIR and SOURCE_DIR directories should NEVER be necessary for regular mods
    def install(self):
        os.rmdir(self.INSTALL_DIR)
        path = self.INSTALL_DIR
        os.makedirs(os.path.dirname(path), exist_ok=True)

        # Detect morrowind installation and create link to it in install dir
        morrowind_bsa = "Morrowind.bsa"
        morrowind_esm = "Morrowind.esm"
        bloodmoon_esm = "Bloodmoon.esm"
        bloodmoon_bsa = "Bloodmoon.bsa"
        tribunal_esm = "Tribunal.esm"
        tribunal_bsa = "Tribunal.bsa"

        print("Searching for Morrowind installation...")
        for root, dirs, files in os.walk(os.path.abspath(os.sep)):
            if morrowind_bsa in files and morrowind_esm in files:
                print("Found morrowind in directory {}".format(root))
                os.symlink(root, path)
                print("Creating link {} -> {}".format(root, path))

                # Add a global use for bloodmoon and tribunal if they are found.
                # This is a little sneaky, but adding them as local use flags could make the dependency resolver
                #   try to reinstall morrowind with them enabled
                if bloodmoon_esm in files and bloodmoon_bsa in files:
                    add_use("bloodmoon")
                if tribunal_esm in files and tribunal_bsa in files:
                    add_use("tribunal")
                break

        config = read_config()
        for install_dir in self.INSTALL_DIRS:
            self.update_config(config, install_dir)
        write_config(config)

    # Default uninstall uses rmtree, which dislikes symlinks
    def uninstall(self):
        if os.path.exists(self.INSTALL_PATH):
            os.unlink(self.INSTALL_PATH)
        Pybuild1.uninstall(self)
