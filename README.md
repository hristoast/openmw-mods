# The OpenMW Mod Repository
OpenMMR stores pybuilds, a variation on [Gentoo's Ebuilds](https://wiki.gentoo.org/wiki/Ebuild).
These build files can be used by OpenMMM 2.0 to install OpenMW mods.

An overview of the format can be found in the skel.pybuild file.
A way of generating pybuilds will be available as part of OpenMMM.
